let mod,
    result,
    input

    
function oddOrEven(input) {
    mod = input % 2
    return (mod === 0) ?  true : false
}
    
input = 59
result = (oddOrEven(input) === true) ?
    `${input} is an even number.` :
    `${input} is an odd number.` 
console.log(result)